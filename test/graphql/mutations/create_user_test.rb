require 'test_helper'

class Mutations::CreateUserTest < ActiveSupport::TestCase
  
  def perform(user: nil, **args)
    Mutations::Users::CreateUser.new(object: nil, field: nil, context: {}).resolve(args)
  end

  test 'create a new user' do
    user = perform(
      first_name: 'ciaran',
      last_name: 'campbell',
      email: 'ciri@me.com',
    )

    assert user.persisted?
    assert_equal user.first_name, 'ciaran'
    assert_equal user.last_name, 'campbell'
    assert_equal user.email, 'ciri@me.com'
  end

  # test 'user must have first name' do
  #   user = perform(
  #     last_name: 'campbell',
  #     email: 'ciri@me.com',
  #   )

  #   assert_not user.persisted?
  # end


end
