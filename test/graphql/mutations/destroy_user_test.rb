require 'test_helper'

class Mutations::DestroyUserTest < ActiveSupport::TestCase


  def setup
    @user = User.first
  end

  
  def perform(user: nil, **args)
    Mutations::Users::DestroyUser.new(object: nil, field: nil, context: {}).resolve(args)
  end

  test 'destroy a user' do
    
    user = perform(
      id: @user.id,
    )
    
    assert_not User.exists?(@user.id)
   
  end

  

end
