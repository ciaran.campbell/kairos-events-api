require 'test_helper'

class Mutations::UpdateEventTest < ActiveSupport::TestCase


  def setup
    @event = Event.first
    @start_time = DateTime.now
    @end_time = DateTime.now + 1.year
  end

  
  def perform(user: nil, **args)
    Mutations::Events::UpdateEvent.new(object: nil, field: nil, context: {}).resolve(args)
  end

  test 'update an event' do
    event = perform(
      id: @event.id,
      title: 'test event',
      start_time: @start_time, 
      end_time: @end_time,
    )
    
    assert event.persisted?
    assert_equal event.title, 'test event'
    assert_equal event.start_time, @start_time
    assert_equal event.end_time, @end_time
  end

  

end
