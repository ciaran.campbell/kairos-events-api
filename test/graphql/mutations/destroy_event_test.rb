require 'test_helper'

class Mutations::DestroyEventTest < ActiveSupport::TestCase


  def setup
    @event = Event.first
  end

  
  def perform(user: nil, **args)
    Mutations::Events::DestroyEvent.new(object: nil, field: nil, context: {}).resolve(args)
  end

  test 'destroy an event' do
    
    event = perform(
      id: @event.id,
    )
    
    assert_not Event.exists?(event.id)
   
  end

  

end
