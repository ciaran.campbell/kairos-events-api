require 'test_helper'

class Mutations::CreateEventTest < ActiveSupport::TestCase


  def setup
    @start_time = DateTime.now
    @end_time = DateTime.now + 1.year
  end

  
  def perform(user: nil, **args)
    Mutations::Events::CreateEvent.new(object: nil, field: nil, context: {}).resolve(args)
  end

  test 'create a new event' do
    event = perform(
      title: 'test event',
      start_time: @start_time,
      end_time: @end_time,
      user_id: 1,
    )

    assert event.persisted?
    assert_equal event.title, 'test event'
    assert_equal event.start_time, @start_time
    assert_equal event.end_time, @end_time
  end

  

end
