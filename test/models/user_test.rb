require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users(:one)
  end


  test "should be valid" do
    assert @user.valid?
  end


  test "user should have first name" do
     @user.first_name = ""
     assert_not @user.valid?
  end


  test "user should have last name" do
     @user.last_name = ""
     assert_not @user.valid?
  end


  test "user should have valid email" do
     @user.email = ""
     assert_not @user.valid?
     @user.email = "sdfsdf"
     assert_not @user.valid?
     @user.email = "sd@sdf.com"
     assert @user.valid?
  end


end
