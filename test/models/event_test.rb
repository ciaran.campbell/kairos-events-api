require 'test_helper'

class EventTest < ActiveSupport::TestCase
  def setup
    @event = events(:one) 
  end


  test "should be valid" do
    assert @event.valid?
  end


  test "event should have title" do
     @event.title = ""
     assert_not @event.valid?
  end


  test "event should have valid start time" do
     @event.start_time = ""
     assert_not @event.valid?
     @event.start_time = "kjjh"
     assert_not @event.valid?
     @event.start_time = DateTime.now
     assert @event.valid?
  end

  
  test "event should have valid end time" do
     @event.end_time = ""
     assert_not @event.valid?
     @event.end_time = "dgdgf"
     assert_not @event.valid?
     @event.end_time = DateTime.now
     assert @event.valid?
  end


  test "event end time should be after start time" do
     @event.end_time = DateTime.now - 1.year
     assert_not @event.valid?
     @event.end_time = DateTime.now + 1.year
     assert @event.valid?
  end


  test "event should have a valid (existing) associated user" do
     @event.user_id = nil
     assert_not @event.valid?
     @event.user_id = 500
     assert_not @event.valid?
  end 
  
end
