# README

* Ruby version 2.6.3


To run type 'rails s' into terminal in home dir of app

To view/test in GraphiQL go to:  localhost:3000/graphiql

==============================================================================


#Querys/Mutations


**Create a Calendar Event**

'mutation{
  createEvent(title: "New Event", userId: 11,
     startTime:"2007-12-03T10:15:30Z", 
     endTime: "2007-12-03T10:15:30Z")
  {
    id    
  }
}'




**Destroy a Calendar Event**

'mutation{
  destroyEvent(id: 1008)
  {
    id    
  }
}'




**Update a Calendar Event**


'mutation{
  updateEvent(id: 1007, title: "New Title")
  {
    id    
  }
}'




**Create a User**


'mutation{
  createUser(firstName: "Ciaran", lastName: "Campbell", 
              email: "ciaran.campbell@mail.com")
  {
    id    
  }
}'



**Destroy a User**

'mutation{
  destroyUser(id: 10)
  {
    id    
  }
}'





**View calendar events for specific users between given dates**


'{
  user(id: 12){
    id
    events(startTime: "2020-12-03T10:15:30Z", endTime: "2020-12-04T10:15:30Z", limit: 3){
      title
    }
  }
}
'





**View List of Users for Event**


'{
  event(id: 1)
  {
   invitedUsers{
    user{
      email
    }
  }
 }
}'

=========================================================================================================



# Summary/Additional Info


**N+1 Query Issue**

Implemented data loaders to lazily load all data that comes from ActiveRecord relations.
Used graphql-batch gem which allows queries to be batched.




**Testing**

Installed Guard for Automated Testing

To run:

'bundle exec guard'

Testing is very limited..only testing successful API Calls
Should implement more detailed testing




**Security**

Should implement measures to protect against malicous clients:

Timeouts
Max Query Depth
Throttling
