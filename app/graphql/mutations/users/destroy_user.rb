module Mutations
  module Users
    class DestroyUser < ::Mutations::BaseMutation
      argument :id, Integer, required: true
      
      type Types::UserType

      def resolve(id:)
        User.find(id).tap do |user|
          user.destroy!
        end
      end
    end
  end
end