module Mutations
  module Events
    class CreateEvent < ::Mutations::BaseMutation
      # arguments passed to the `resolve` method
      argument :start_time, GraphQL::Types::ISO8601DateTime, required: true
      argument :end_time, GraphQL::Types::ISO8601DateTime, required: true
      argument :title, String, required: true
      argument :user_id, Integer, required: true

      # return type from the mutation
      type Types::EventType

      def resolve(start_time: nil, end_time: nil, title: nil, user_id: nil)
        Event.create!(
          start_time: start_time,
          end_time: end_time,
          title: title,
          user_id: user_id,
        )
      end
    end
  end
end