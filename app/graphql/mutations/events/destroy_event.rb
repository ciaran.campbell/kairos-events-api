module Mutations
  module Events
    class DestroyEvent < ::Mutations::BaseMutation
      argument :id, Integer, required: true
      
      type Types::EventType

      def resolve(id:)
        Event.find(id).tap do |event|
          event.destroy!
        end
      end
    end
  end
end