module Mutations
  module Events
    class UpdateEvent < ::Mutations::BaseMutation
      argument :id, Integer, required: true
      argument :start_time, GraphQL::Types::ISO8601DateTime, required: false
      argument :end_time, GraphQL::Types::ISO8601DateTime, required: false
      argument :title, String, required: false
      argument :user_id, Integer, required: false

      type Types::EventType

      def resolve(id:, **attributes)
        Event.find(id).tap do |event|
          event.update!(attributes)
        end
      end
    end
  end
end