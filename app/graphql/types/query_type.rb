module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # /users
    field :users, [Types::UserType], null: false

    def users
      User.all
    end

    # /user/:id
    field :user, Types::UserType, null: false do
      argument :id, ID, required: true
    end

    def user(id:)
      User.find(id)
    end


    

    # /events
    field :events, [EventType], null: false
    
    
    def events
      Event.all
    end


    # /event/:id
    field :event, Types::EventType, null: false do
      argument :id, ID, required: true
    end

    def event(id:)
      Event.find(id)
    end


    field_events

    def events(**query_options)
      Event.graphql_query(query_options)
    end 
    

    

  end
end
