module Types
  class MutationType < Types::BaseObject
    field :create_event, mutation: Mutations::Events::CreateEvent
    field :update_event, mutation: Mutations::Events::UpdateEvent
    field :destroy_event, mutation: Mutations::Events::DestroyEvent
    field :create_user, mutation: Mutations::Users::CreateUser
    field :destroy_user, mutation: Mutations::Users::DestroyUser
  end
end
