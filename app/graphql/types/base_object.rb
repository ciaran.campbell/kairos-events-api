module Types
  class BaseObject < GraphQL::Schema::Object
    field_class Types::BaseField


    def self.field_events
      field :events, [Types::EventType], null: false do
        argument :start_time, GraphQL::Types::ISO8601DateTime, required: false
        argument :end_time, GraphQL::Types::ISO8601DateTime, required: false
        argument :limit, Integer, required: false
      end
    end


  end
end
