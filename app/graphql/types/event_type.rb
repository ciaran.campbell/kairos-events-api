module Types
  class EventType < Types::BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :user_id, Integer, null: false
    field :start_time, GraphQL::Types::ISO8601DateTime, null: false
    field :end_time, GraphQL::Types::ISO8601DateTime, null: false
    field :user, Types::UserType, null: false
    field :invited_users, [Types::InvitedUserType], null: false

    def user
      Loaders::BelongsToLoader.for(User).load(object.user_id)
    end

  end


  

end
