module Types
  class InvitedUserType < Types::BaseObject
    field :id, ID, null: false
    field :event, Types::EventType, null: false
    field :user, Types::UserType, null: false
  end
end
