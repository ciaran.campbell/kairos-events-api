module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :email, String, null: false
    field :events, [Types::EventType], null: false
    field :event_invites, [Types::InvitedUserType], null: false

   

    field_events

	def events(**query_options)
	  Loaders::HasManyLoader
	  .for(Event, :user_id, query_options)
	  .load(object.id)
	end

  end


  


end
