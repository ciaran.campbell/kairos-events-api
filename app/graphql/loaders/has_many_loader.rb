class Loaders::HasManyLoader < GraphQL::Batch::Loader
  def initialize(model, column, query_options)
    @model = model
    @column = column
    @query_options = query_options
  end

  def perform(relation_ids)
    query = @model.graphql_query(@query_options).where({@column => relation_ids})

    records_by_relation_id = query.group_by { |result| result.public_send(@column) }

    relation_ids.each do |id|
      fulfill(id, records_by_relation_id[id] || [])
    end
  end
end
