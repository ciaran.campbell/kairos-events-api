class User < ApplicationRecord

  has_many :events, dependent: :destroy
  has_many :event_invites, class_name: "InvitedUser"
  
  
  # user validations
  validates_presence_of :first_name
  validates_presence_of :last_name
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 


  def filter_events(start_date, end_date)
  	self.events.where(id: 3)
  end

end
