class Event < ApplicationRecord
  belongs_to :user, validate: true
  has_many :invited_users
  # has_many :users, through: :invited_users

   # event validations
  validates_presence_of :title 
  validates_presence_of :start_time
  validates_presence_of :end_time

  validate :end_after_start?


  def self.graphql_query(options)
    Event.where(start_time: options[:start_time]..options[:end_time]).limit(options[:limit])
  end



  private
    def end_after_start?
    	return if end_time.blank? || start_time.blank?
        
        if end_time < start_time
          errors.add(:end_date, "must be after the start date") 
        end
    end
end
